# habi-services

Prueba técnica Servicios para tu habi 

## Description
Este proyecto tiene como objetivo brindar los siguientes servicios:

    -Recuperación de inmuebles con los siguientes filtros de estado, año de construcción, ciudad, estado ya sea individuales o varios a la vez.

    -Visualización de la siguiente información:
        -Dirección
        -Ciudad
        -Estado
        -Precio de venta
        -Descripción

Para lograr esto se implementará una estructura de la siguiente manera:
    main : Donde se levanta el servicio con ayuda del wsgi
    habi_services: Carpeta donde estara la estructura de los microservicios
        -workers: Dentro de esta carpeta estarán las estructuras de cada uno de los  microservicios
        -db: Es un helper para la conexión a la BD
        -Cada worker tendra su archivo de python para el microservicio y su DAO 

Las dependencias de las librerías del proyecto se definen en el fichero requirements.txt 

Se utilizarán las tecnologias siguientes:

    -Python versión 3.10 como lenguaje de programación.
    -wsgi para levantar el servidor.
    -Para la conexión a la base de datos utilizaremos la libreria mysql-connector-python
    -pytest para las pruebas unitarias.
    -Para realizar los servicios de comunicación emplearemos una comunicación
basada en el modelo REST bajo HTTP.
    
    

    **Desarrollo**
    
    La funcionalidad se basa en que el archivo main.py se reciba la solicitud de cada uno de los microservicios y tanto la funcionalidad como el proceso de consulta a la BD este contenido dentro de la carpeta de cada uno de los microservicios en la cual se realizara la ejecución de la consulta y se devolvera la respuesta por cada servicio.

##JSON 
    Dentro del proyecto se encuentra un archivo JSON donde se encuentran los datos que se esperan desde el front.


## Installation


**Primero crear el ambiente virtual con el siguiente comando**
```
python3 -m venv habi-services
```

**Activar el ambiente virtual**
source env/bin/activate

**instalar librerias y requerimientos**
```
pip install -r requirements.txt
```

**Configuración de variables de entorno**

agregar un archivo .env en la raiz del proyecto y agregar las variables de entorno siguientes con sus valores correspondientes:

DB_HOST = host_db
DB_NAME = schema_name_db
DB_USER = user_db
DB_PASSWORD = passwrod_db
DB_PORT = port_db

**Ejecución del servicio**
python main.py


**Ejecución de pruebas unitarias**
ejecutar el comando "pytest" con el ambiente virtual activo



##Servicio 2 "Me gusta"

**Diagrama E-R**

![diagrama e-r](Diagrama_ER_tuhabi-likes.png)



**Código SQL **

Creación de tabla likes

CREATE TABLE IF NOT EXISTS `habi_db`.`likes` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `like` TINYINT(1) NOT NULL,
  `timestamp` DATETIME NULL DEFAULT NULL,
  `liked_property_id` INT NOT NULL,
  `liked_user_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `likes_id_uindex` (`id` ASC) VISIBLE,
  INDEX `likes_property_id_fk_idx` (`liked_property_id` ASC) VISIBLE,
  INDEX `likes_user_id_fk_idx` (`liked_user_id` ASC) VISIBLE,
  CONSTRAINT `likes_property_id_fk`
    FOREIGN KEY (`liked_property_id`)
    REFERENCES `habi_db`.`property` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `likes_user_id_fk`
    FOREIGN KEY (`liked_user_id`)
    REFERENCES `habi_db`.`auth_user` (`id`)
)

Creación de tabla property_likes

CREATE TABLE IF NOT EXISTS `habi_db`.`property_likes` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `property_id` INT NOT NULL,
  `user_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `likes_id_uindex` (`id` ASC) VISIBLE,
  INDEX `likes_user_id_fk0_idx` (`user_id` ASC) VISIBLE,
  CONSTRAINT `property_likes_property_id_fk0`
    FOREIGN KEY (`property_id`)
    REFERENCES `habi_db`.`property` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `property_likes_user_id_fk0`
    FOREIGN KEY (`user_id`)
    REFERENCES `habi_db`.`auth_user` (`id`)
)


**Explicación del por que de las tablas**


Agregue la tabla **likes** para almacenar la información de los "Me gusta" realizados por propiedad, almacenando el id de la propiedad, el usuario, la fecha que le dio "Me gusta" con el campo 'like'. Y esta preparada para un "Me gusta" o "No me gusta" al ser un booleando el campo 'like'.


Agregue la tabla pivote **property_likes** para poder almacenar cuantos "Me gusta" se han realizado a una propiedad, almacenando esta informaciñon por cada propiedad el usuario que le dio like.



**Puntos Extra**

**Diagrama E-R Optimizacióm**

![diagrama e-r optimization](Diagrama_ER_tuhabi-optimization.png)

En este caso agregaria una tabla mas de "Ciudades" para almacenar las ciudades y al momento de realizar el filtro de las propiedades por ciudad, se filtre por id y no por nombre para de esta manera realizar la busqueda de una manera mas rapida, al realizar esto se proporcionaria el catalogo al front para que puedan obtener los ids de las ciudades.
