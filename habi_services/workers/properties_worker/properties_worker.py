import json
from .properties_dao import PropertiesDAO


class WorkerProperties:
    """
    name: WorkerProperties
    Worker para la recuperación de información de propiedaes.
    list: Lista todas las propiedades con el ultimo estatus.
    filters: Los filtros que se pueden usar son la ciudad, el estado y el año de construcción.
    """

    __propertiesdao = None

    def __init__(self):
        self.__propertiesdao = PropertiesDAO()

    def getProperties(self, filters):
        """
        Método para recuperar las propiedades
        Params: Recibe como parametros para filtrado:
                estado: Filtra por el último estatus de la propiedad.
                ciudad: Filtra por el campo city.
                anio: Filtra por el campo year correspondiente al año de construcción.
        """
        if filters is None:
            properties = self.__propertiesdao.getProperties()
            return properties
        else:
            str_filters = ""
            array_filters_and = filters.split("&")
            for filters in array_filters_and:
                key_value_filter = filters.split("=")
                key = key_value_filter[0].strip()
                value = key_value_filter[1].strip()
                if key == "estado":
                    str_filters = str_filters + "name = " + "'" + value + "'"
                if key == "ciudad":
                    if len(str_filters) > 1:
                        str_filters = str_filters + " AND "
                    str_filters = str_filters + "city = " + "'" + value + "'"
                if key == "anio":
                    if len(str_filters) > 1:
                        str_filters = str_filters + " AND "
                    str_filters = str_filters + "year = " + value

            properties = self.__propertiesdao.getPropertiesWithFilters(str_filters)
            return properties
