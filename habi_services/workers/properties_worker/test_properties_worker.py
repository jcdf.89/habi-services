import os
import json
from pytest import MonkeyPatch as monkeypatch
from .properties_worker import WorkerProperties


def test_properties_worker_get_all_properties(monkeypatch):
    envs = {
        "DB_HOST": "3.130.126.210",
        "DB_NAME": "habi_db",
        "DB_USER": "pruebas",
        "DB_PASSWORD": "VGbt3Day5R",
        "DB_PORT": 3309,
    }
    monkeypatch.setattr(os, "environ", envs)

    properties_worker = WorkerProperties()
    properties = properties_worker.getProperties(None)
    assert len(properties) > 0


def test_properties_worker_not_get_properties_not_exist_state(monkeypatch):
    envs = {
        "DB_HOST": "3.130.126.210",
        "DB_NAME": "habi_db",
        "DB_USER": "pruebas",
        "DB_PASSWORD": "VGbt3Day5R",
        "DB_PORT": 3309,
    }
    monkeypatch.setattr(os, "environ", envs)
    filters = "estado=comprado"
    properties_worker = WorkerProperties()
    properties = properties_worker.getProperties(filters)
    assert len(properties) == 0


def test_properties_worker_get_properties_by_state(monkeypatch):
    envs = {
        "DB_HOST": "3.130.126.210",
        "DB_NAME": "habi_db",
        "DB_USER": "pruebas",
        "DB_PASSWORD": "VGbt3Day5R",
        "DB_PORT": 3309,
    }
    monkeypatch.setattr(os, "environ", envs)
    filters = "estado=pre_venta"
    properties_worker = WorkerProperties()
    properties = properties_worker.getProperties(filters)
    count_properties = len(properties)
    count_properties_with_same_state = 0

    for propertie in properties:
        json_object = json.loads(propertie)
        if json_object["property"]["estado"] == "pre_venta":
            count_properties_with_same_state = count_properties_with_same_state + 1
    assert count_properties == count_properties_with_same_state


def test_properties_worker_get_properties_by_city(monkeypatch):
    envs = {
        "DB_HOST": "3.130.126.210",
        "DB_NAME": "habi_db",
        "DB_USER": "pruebas",
        "DB_PASSWORD": "VGbt3Day5R",
        "DB_PORT": 3309,
    }
    monkeypatch.setattr(os, "environ", envs)
    filters = "ciudad=bogota"
    properties_worker = WorkerProperties()
    properties = properties_worker.getProperties(filters)
    count_properties = len(properties)
    count_properties_with_same_city = 0

    for propertie in properties:
        json_object = json.loads(propertie)
        if json_object["property"]["ciudad"] == "bogota":
            count_properties_with_same_city = count_properties_with_same_city + 1
    assert count_properties == count_properties_with_same_city


def test_properties_worker_get_properties_by_year(monkeypatch):
    envs = {
        "DB_HOST": "3.130.126.210",
        "DB_NAME": "habi_db",
        "DB_USER": "pruebas",
        "DB_PASSWORD": "VGbt3Day5R",
        "DB_PORT": 3309,
    }
    monkeypatch.setattr(os, "environ", envs)
    filters = "anio=2000"
    properties_worker = WorkerProperties()
    properties = properties_worker.getProperties(filters)
    count_properties = 5
    count_properties_with_same_year = 0

    for propertie in properties:
        count_properties_with_same_year = count_properties_with_same_year + 1
    assert count_properties == count_properties_with_same_year
