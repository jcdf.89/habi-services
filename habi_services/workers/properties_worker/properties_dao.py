import json

from .models.properties import Property
from .db_connection import ManagerConnectionToDB


class PropertiesDAO:
    """
    DAO para la instancia de Propiedades
    -Utiliza el ManagerConnectionDB para la conexión a la BD
    """

    __db = None

    def __init__(self):
        self.__db = ManagerConnectionToDB()

    def getProperties(self):
        """
        Función para la ejecución de la query
        correspondiente para la recuperación de todas las propiedades
        que cumplan con los criterios siguientes:
            -Se recuperan todas las propiedades cuyo estado sea 'pre_venta'
                'en_venta'y 'vendido'
            -El filtro se realiza por el ultimo estatus de la propiedad.
        """
        properties = []
        records = self.__db.query(
            """
            SELECT p.id, p.address, p.city, sh.status_id, s.name, p.price, p.description, p.year
            FROM habi_db.property p 
            LEFT JOIN habi_db.status_history sh  
            ON sh.property_id = p.id
            LEFT JOIN habi_db.status s  
            ON sh.status_id = s.id
            WHERE sh.update_date = (SELECT max(update_date) FROM habi_db.status_history WHERE property_id = p.id)
            AND name IN ('pre_venta', 'en_venta', 'vendido')
            GROUP BY p.id;""",
            None,
        ).fetchall()

        for record in records:
            propertie = Property()
            propertie.direccion = record[1]
            propertie.ciudad = record[2]
            propertie.estado = record[4]
            propertie.precio_venta = record[5]
            propertie.descripcion = record[6]
            properties.append(json.dumps(propertie.__dict__))
        return properties

    def getPropertiesWithFilters(self, filters):
        """
        Función para la ejecución de la query con filtros
        correspondiente para la recuperación de todas las propiedades
        que cumplan con los criterios siguientes:
            -Se recuperan todas las propiedades cuyo estado sea 'pre_venta'
                'en_venta'y 'vendido'.
            -El filtro se realiza por el ultimo estatus de la propiedad.
            -Esta función permite el filtrado de las propiedades
             por los siguientes criterios:
             -Ciudad
             -Estado
             -Año de construcción.
        """
        sql_filter = (
            f"""
        SELECT p.id, p.address, p.city, sh.status_id, s.name, p.price, p.description, p.year
            FROM habi_db.property p 
            LEFT JOIN habi_db.status_history sh  
            ON sh.property_id = p.id
            LEFT JOIN habi_db.status s  
            ON sh.status_id = s.id
            WHERE sh.update_date = (SELECT max(update_date) FROM habi_db.status_history WHERE property_id = p.id)
            AND name IN ('pre_venta', 'en_venta', 'vendido')
            AND """
            + filters
            + """
            GROUP BY p.id;
        """
        )

        properties = []
        records = self.__db.query(
            sql_filter,
            None,
        ).fetchall()

        for record in records:
            propertie = Property()
            propertie.direccion = record[1]
            propertie.ciudad = record[2]
            propertie.estado = record[4]
            propertie.precio_venta = record[5]
            propertie.descripcion = record[6]
            json_property = {"property": propertie.__dict__}
            properties.append(json.dumps(json_property))
        return properties
