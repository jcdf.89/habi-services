from multiprocessing import connection
import os
import mysql.connector
from mysql.connector import Error


class ManagerConnectionToDB:
    """
    Helper creado para la conexión a la BD, que lee
    de las variables de entorno las configuraciones para
    la conexión.
    """

    __connection = None
    __cursor = None

    def __init__(self):
        try:
            self.__connection = mysql.connector.connect(
                host=os.getenv("DB_HOST"),
                database=os.getenv("DB_NAME"),
                user=os.getenv("DB_USER"),
                password=os.getenv("DB_PASSWORD"),
                port=os.getenv("DB_PORT"),
            )

            if self.__connection.is_connected():
                db_Info = self.__connection.get_server_info()
                print("Connected to MySQL Server version ", db_Info)
                self.__cursor = self.__connection.cursor()
                self.__cursor.execute("select database();")
                record = self.__cursor.fetchone()
                print("You're connected to database: ", record)
        except Error as e:
            print("Error while connecting to MySQL", e)

    def query(self, query, params):
        self.__cursor.execute(query, params)
        return self.__cursor

    def close_connection(self):
        if self.__connection.is_connected():
            self.__cursor.close()
            self.__connection.close()
            print("MySQL connection is closed")
