import json
from urllib import response
from wsgiref.simple_server import make_server

from workers.properties_worker.properties_worker import WorkerProperties


def home(environ):
    return {"mensaje": "Hola mundo desde el servidor"}


def application(environ, start_response):

    path = environ.get("PATH_INFO")
    if path.endswith("/"):
        path = path[:-1]
    if path == "":
        data = home(environ)
    if path == "/properties":
        filters = None
        __workproperties = WorkerProperties()
        if environ.get("QUERY_STRING"):
            filters = environ.get("QUERY_STRING")
        data = __workproperties.getProperties(filters)
    headers = [("Content-type", "application/json")]

    start_response("200 OK", headers)

    return [bytes(json.dumps(data), "utf-8")]


server = make_server("localhost", 8000, application)
server.handle_request()
